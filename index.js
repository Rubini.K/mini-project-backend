const express = require('express')
const app = express()

const bodyParser=require('body-parser');
const errorController = require('./controllers/error');
//   const productRoutes = require('./routes/product');
const productRoutes = require('./routes/product');
const cartRoutes = require('./routes/cart');

const ports = process.env.PORT || 3001;

app.use(bodyParser.json());
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
  });

 app.use('/product',productRoutes);
//  app.use('/product/p',pizzaRoutes);
app.use('/cart',cartRoutes);

 app.use(errorController.get404);
 app.use(errorController.get500);

app.listen(ports,()=>{
    console.log('running');
});