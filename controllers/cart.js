const Cart = require('../models/cart');
const db=require('../db/database');

exports.findAll = (req, res,next) => {
  try{
    //const [allProducts] = await Product.fetchAll();
    db.query('SELECT * from Cart', function (err, cart, fields) {
      if (err) throw err;
      //return console.log(result);
      return res.status(200).json({cart});
    });
    
   // res.status(200).json(allProducts);
  }catch(err){
      if(!err.statusCode){
          err.statusCode=500;
      }
      next(err);
  }
};
exports.allCarts = (req, res,next) => {
  try{
    //const [allProducts] = await Product.fetchAll();
    db.query('SELECT * from carts_tb', function (err, cart, fields) {
      if (err) throw err;
      //return console.log(result);
      return res.status(200).json({cart});
    });
    
   // res.status(200).json(allProducts);
  }catch(err){
      if(!err.statusCode){
          err.statusCode=500;
      }
      next(err);
  }
};

exports.create = (req,res, next) =>{ 
  const id = req.body.id;
    let quantity = req.body.quantity;
    try{
      //const [allProducts] = await Product.fetchAll();
      db.query('INSERT INTO Cart (id,quantity) VALUES (?)',[[id,quantity]], function (err, result) {
        if (err) throw err;
        //return console.log(result);
        return res.status(200).json({result});
      });
      
     // res.status(200).json(allProducts);
    }catch(err){
        if(!err.statusCode){
            err.statusCode=500;
        }
        next(err);
    }
   
   
       
        
};

exports.createNewCart = (req,res, next) =>{ 
   const id = req.body.id;
    let name = req.body.name;
    try{
      //const [allProducts] = await Product.fetchAll();
      db.query('INSERT INTO carts_tb (id,name) VALUES (?)',[[id,name]], function (err, result) {
        if (err) throw err;
        //return console.log(result);
        return res.status(200).json({result});
      });
      
     // res.status(200).json(allProducts);
    }catch(err){
        if(!err.statusCode){
            err.statusCode=500;
        }
        next(err);
    }
   
   
       
        
};

exports.updateAndDelete = (req,res) => {
  const id = req.params.id;
  if(req.body.quantity == 0 ){
    Cart.destroy({
      where: { productId: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Cart was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Cart with id=${id}. Maybe Cart was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Cart with id=" + id
        });
      });
  }else {
    Cart.update(req.body, {
      where: { productId: id }
  })
      .then(num => {
          if (num == 1) {
              res.send({
                  message: "Cart was updated successfully."
              });
          } else {
              res.send({
                  message: `Cannot update Cart with id=${id}. Maybe Cart was not found or req.body is empty!`
              });
          }
      })
      .catch(err => {
          res.status(500).send({
              message: "Error updating Cart with id=" + id
          });
      });
  }
};

exports.update = (req, res) => {

    const id = req.params.id;

    Cart.update(req.body, {
        where: { productid: id }
    })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Cart was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Cart with id=${id}. Maybe Cart was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Cart with id=" + id
            });
        });
};

exports.delete = (req, res) => {

    const id = req.params.id;
    try{
      
        db.query('DELETE FROM Cart WHERE id = ?', [id], function (err, result) {
          if (err) throw err;
        return res.status(200).json({result});
        
      });
      
     // res.status(200).json(allProducts);
    }catch(err){
        if(!err.statusCode){
            err.statusCode=500;
        }
        next(err);
    }
  
};

exports.deleteCart = (req, res) => {

  const id = req.params.id;
  try{
    
      db.query('DELETE FROM carts_tb WHERE id = ?', [id], function (err, result) {
        if (err) throw err;
      return res.status(200).json({result});
      
    });
    
   // res.status(200).json(allProducts);
  }catch(err){
      if(!err.statusCode){
          err.statusCode=500;
      }
      next(err);
  }

};

exports.deleteAll = (req, res) => {

    const id = req.params.id;

    Cart.destroy({
        where: { userId: id }
      })
        .then(num => {
          if (num == 1) {
            res.send({
              message: "Cart was deleted successfully!"
            });
          } else {
            res.send({
              message: `Cannot delete Cart with id=${id}. Maybe Cart was not found!`
            });
          }
        })
        .catch(err => {
          res.status(500).send({
            message: "Could not delete Cart with id=" + id
          });
        });
  
};

