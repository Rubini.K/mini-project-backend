const Product = require('../models/products');
const db=require('../db/database')

exports.getAllProducts = async(req, res, next) => {
    try{
      //const [allProducts] = await Product.fetchAll();
      db.query('SELECT * from products', function (err, result, fields) {
        if (err) throw err;
        //return console.log(result);
        return res.status(200).json({result});
      });
      
     // res.status(200).json(allProducts);
    }catch(err){
        if(!err.statusCode){
            err.statusCode=500;
        }
        next(err);
    }
};
exports.getProductById = async(req, res, next) => {
  const id = req.params.id;
  console.log(id);
  try{
  var sql='SELECT * from products where productid = ?';
    db.query('SELECT * from products where productid = ?', [id], function (err, result) {
      if (err) throw err;
    return res.status(200).json({result});
    
  });
  
 // res.status(200).json(allProducts);
}catch(err){
    if(!err.statusCode){
        err.statusCode=500;
    }
    next(err);
}

};