const express = require('express');

const cartController = require('../controllers/cart');

const router = express.Router();

router.get("/", cartController.findAll); 

    router.get("/carts", cartController.allCarts); 

    router.post("/", cartController.create);

    router.post("/carts", cartController.createNewCart);

    router.put("/pro/:id", cartController.updateAndDelete);

    router.put("/:id", cartController.update);

    router.delete("/:id", cartController.delete);
    router.delete("/carts/:id", cartController.deleteCart);

    router.delete("/user/:id", cartController.deleteAll);
  
      

module.exports = router;