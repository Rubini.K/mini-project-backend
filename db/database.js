const mysql=require('mysql');

const config = require('../config/config.json');

const pool=mysql.createPool({
    host:config.host,
    user:config.user,
    password:config.password,
    database:config.database,
});

pool.query('SELECT 1 + 1 AS solution', (error, results, fields) => {
    if (error) throw error;
    console.log('The solution is: ', results[0].solution);
  });

module.exports = pool;